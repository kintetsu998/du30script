# Du30 Script 

Du30 Script is an interpreted programming language that is inspired by the one and only Rodrigo R. Duterte, the Best President of the Philippines. This programming language is also inspired by the [TrumpScript](https://github.com/samshadwell/TrumpScript).

## Installation 

Fill this up after finishing the interpreter.

----------

## Commenting 

Tatay Digong always comments on various popular figures like Pope Francis I, organizations like the EU, and lastly issues like drugs. Therefore, this programming language will support the feature of commenting in your code.

### Multi-line Comment 

President Duterte, just like any other presidents, likes to speak tons of words in their State of the Nation Address (SONA). Also, just like any other SONA, there will people who will applause at the achievements of the president. Therefore, du30 script is designed to implement the multiline comment in the same way.

``` 
SONA
My fellow citizens... much remains to be done. Corruption persists like a fishbone stuck in the throat; 
it pains and it is disconcerting. We need to pry corruption from government corpus which is deeply 
embedded and we also need to put an end to squabbles and bickerings with an agencies focused truly on 
speedy provision of quality public services to our people.
[applause]
```

### Single-line Comment 

The Filipino President, like other Filipinos, is a happy person. He still smiles and laughs even if his son is suspected to be involved in a drug cartel. He still has a joy in his heart even if there are [4 Million drug users](http://www.philstar.com/news-feature/2017/02/07/1669938/4m-drug-users-realm-possibility-ddb-insists) living happily inside the Philippines. 

_"Masayahin siya,"_ his fellow countrymen would say. Therefore, Digong also likes to share his jokes from time to time. To create a single line comment in du30 script, just add `joke lang` at the end of the line like how the president wants to tell his jokes.

```
Maybe we'll just have to decide to separate from the United Nations. joke lang
```

----------

## Typing

The Philippine President only knows two kinds of people: Innocents and Users. This language only knows two kinds of values: Numbers and Strings. 

Booleans are also supported by this language. However, they are represented by numbers: greater than or equal to 1 being evaluated to true, less than 1 is evaluated to false. There are special keywords used in this language to easily denote the values of true and false. Keyword `mocha` will already be understood as true while keywords `media`, `Rappler`, `ABS-CBN`, `GMA`, and `Inquirer` are all evaluated as false.

```
1 is a Number
1.5 is a Number
.5 is a Number
"Duterte" is a String
"Marcos" is a String
"25" is a String
mocha is a Boolean value evaluated as true 
media is a Boolean value evaluated as false
```

## Variables

Variables are no longer needed to be declared before being used. They are automatically declared if they are not found from the list of known variables. 

The default value of a Number is `0` and the default value of a string is an empty string `""`.

The variable names are alphanumeric and are case sensitive. They are also loosely typed.

### Variable Assignment

The president is well known for swearing. Especially when he is saying "_putang ina mo_" (translates to "you are a son of a whore") with extreme passion. Therefore, the script needs to swear with emotions when assigning a value to a variable.

```
assigns 5 to variable foo joke lang
gawin mo ang putanginang foo bilang 5
```

### Operations

Just like the assignment operation, the script needs to swear to conduct the following operations

#### Subtraction

Keyword `bawasan` is used to denote subtraction.

```
subtracts the value of foo by 1 joke lang
bawasan ang putanginang foo ng 1
```

#### Division

Keyword `hatiin` is used to denote subtraction.

```
divides the value of foo by 2 joke lang
hatiin ang putanginang foo ng 2
```

#### Obosen

If you suspect that a variable is indeed a drug user and should be eliminated immediately, use the keyword `obosen` to render the variable unusable. Any interaction done with this variable will return an error from the interpreter.

```
eliminates the variable foo joke lang
obosen foo
```

#### Addition and Multiplication

The Filipino President does not like to add more drug users and drug pushers in the country. Therefore, this language will prevent the value of its variables to increase. If any attempt is done to increase the values of the variables, the interpreter will throw an error. However, the value should not be less than or equal to zero because it will be against the Philippine Constitution.

#### Special Variable

Because Duterte has mercy upon CHR, giving them a budget of PhP 1000 (equivalent to USD 20) instead of abolishing it, the `chr` in this programming language will instantly contain 1000. This value cannot be increased by any means (by assigning, subtracting negative values, or dividing it by fraction values which has a value of less than 1). In doing so, will make the interpreter throw an error.

## Input/Output

If Duterte is a god, he has a prophet named Mocha Uson. Mocha Uson has a blog which serves as a Bible for Duterte followers. Therefore, we are going to use her blog to broadcast a string to the terminal.

```
uson.blog "Grabeh ang MAINSTREAM MEDIA umasal nanaman marketing arm ng rally bukas sa Luneta."
```

You can add `!` at the end of the sentence to add a new line after printing it to the terminal.

```
uson.blog "Hindi po tayo makiki-argue pag dating sa paratang ng Fake News dahil propaganda yan ng mga against sa atin."!
```

Duterte's right hand, Gen. Ronald "Bato" Dela Rosa, is now conducting a nationwide search for drug users and drug pushers to have their blood sacrificed for the president. This operation is known as "_oplan tokhang_" where the police will conduct bag searching in various places and will knock on the doors of the suspected drug users.

To knock on the doors of the terminal, the script will use the keyword `tokhang`.

```
tokhang foo
```

## Invalid Values

President Duterte hates drugs. Therefore, any variables that hold a value related to drugs will instantly trigger the `obosen` operation. Meaning, the variables will become unusable for the rest of the code.

The following are the examples of invalid values:

 - List item
 - drug
 - drug user
 - drug pusher
 - shabu
 - cocaine
 - meth

## Error Values

The president has a passionate hatred against drugs. He also has an intense hate for his enemies. Therefore, if any of the words are found inside the code, the interpreter will instantly throw out an error.

 - human rights
 - dilawan
 - Aquino
 - PNoy
 - Noynoy
 - Trillanes
 - Leni
 - Robredo
 - de Lima
